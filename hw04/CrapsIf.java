//////////////
///Matthew Liu
///9/24/18
///CSE2 Craps If

//Asks user if they want to randomly cast dice or state the two dice they want evaluated
//Using if statements, either randomly generates two integers 1-6, or asks for two integers 1-6 using if statements
//Prints slang terminology for outcome of roll

import java.util.Scanner; //imports scanner class
public class CrapsIf{
  //main method
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); //declares scanner class
    System.out.print("Would you like your dice cast to be random? (Enter true or false):"); //prints question statement
    boolean yesNo = myScanner.nextBoolean(); //constructs instance
    
    if ( yesNo == true ){ //if statement for if yesNo is true
      int firstDie = (int)(Math.random()*5)+1; //randomly selects integer between 1 and 6 for first die
      int secondDie = (int)(Math.random()*5)+1; //randomly selects integer between 1 and 6 for second die
      int diceTotal = firstDie + secondDie; //adds value for first and second die to get dice total
      
      if ( diceTotal == 2 ){ //if statement for if total is 2
        System.out.println("Snake Eyes"); //prints slang terminology
      }
      else if ( diceTotal == 3 ){ //if statement for if total is 3
        System.out.println("Ace Deuce"); //prints slang terminology
      }
      else if ( firstDie == 2 && secondDie == 2 ){ //if statement for if total is 4 and both first and second die are 2
        System.out.println("Hard Four"); //prints slang terminology
      }
      else if ( firstDie == 1 && secondDie == 3 || firstDie == 3 && secondDie == 1 ){ //if statement for if total is 4 and one die is 1 and one die is 3
        System.out.println("Easy Four"); //prints slang terminology
      }
      else if ( diceTotal == 5 ){ //if statement for if total is 5
        System.out.println("Fever Five"); //prints slang terminology
      }
      else if ( firstDie == 3 && secondDie == 3 ){ //if statement for if total is 6 and both dice are 3
        System.out.println("Hard Six"); //prints slang terminology
      }
      else if ( firstDie == 2 && secondDie == 4 || firstDie == 4 && secondDie == 2 || firstDie == 1 && secondDie == 5 || firstDie == 5 && secondDie == 1 ){ //if statement for if total is 6 and neither dice is 3
        System.out.println("Easy Six"); //prints slang terminology
      }
      else if ( diceTotal == 7 ){ //if statement for if total is 7
        System.out.println("Seven Out"); //prints slang terminology
      }
      else if ( firstDie == 4 && secondDie == 4 ){ //if statement for if total is 8 and both dice are 4
        System.out.println("Hard Eight"); //prints slang terminology
      }
      else if ( firstDie == 2 && secondDie == 6 || firstDie == 6 && secondDie == 2 || firstDie == 3 && secondDie == 5 || firstDie == 5 && secondDie == 3){ //if statement for if total is 8 and neither dice is 4
        System.out.println("Easy Eight"); //prints slang terminology
      }
      else if ( diceTotal == 9 ){ //if statement for if total is 9
        System.out.println("Nine"); //prints slang terminology
      }
      else if ( firstDie == 5 && secondDie == 5 ){ //if statement for if total is 10 and both dice are 5
        System.out.println("Hard Ten"); //prints slang terminology
      }
      else if ( firstDie == 4 && secondDie == 6 || firstDie == 6 && secondDie == 4 ){ //if statement for if total is 10 and neither dice is 5
        System.out.println("Easy Ten"); //prints slang terminology
      }
      else if ( diceTotal == 11 ){ //if statement for if total is 11
        System.out.println("Yo-leven"); //prints slang terminology
      }
      else if ( diceTotal == 12 ){ //if statement for if total is 12
        System.out.println("Boxcars"); //prints slang terminology
      }
    }
    else if ( yesNo == false ){ //else if statement for if yesNo is false
      System.out.print("Enter value of first die. (integer between 1 and 6):"); //prints request for value of first die
      int firstDie = myScanner.nextInt(); //constructs instance
      System.out.print("Enter value of second die. (integer between 1 and 6):"); //prints request for value of second die
      int secondDie = myScanner.nextInt(); //constructs instance
      int diceTotal = firstDie + secondDie; //adds first and second dice values
      
      if ( firstDie < 1 || firstDie > 6 || secondDie < 1 || secondDie > 6 ){ //if statement for incorrect inputs
        System.out.println("Incorrect integer input"); //prints incorrect integer input
      }
      else if ( diceTotal == 2 ){ //if statement for if total is 2
        System.out.println("Snake Eyes"); //prints slang terminology
      }
      else if ( diceTotal == 3 ){ //if statement for if total is 3
        System.out.println("Ace Deuce"); //prints slang terminology
      }
      else if ( firstDie == 2 && secondDie == 2 ){ //if statement for if total is 4 and both first and second die are 2
        System.out.println("Hard Four"); //prints slang terminology
      }
      else if ( firstDie == 1 && secondDie == 3 || firstDie == 3 && secondDie == 1 ){ //if statement for if total is 4 and one die is 1 and one die is 3
        System.out.println("Easy Four"); //prints slang terminology
      }
      else if ( diceTotal == 5 ){ //if statement for if total is 5
        System.out.println("Fever Five"); //prints slang terminology
      }
      else if ( firstDie == 3 && secondDie == 3 ){ //if statement for if total is 6 and both dice are 3
        System.out.println("Hard Six"); //prints slang terminology
      }
      else if ( firstDie == 2 && secondDie == 4 || firstDie == 4 && secondDie == 2 || firstDie == 1 && secondDie == 5 || firstDie == 5 && secondDie == 1 ){ //if statement for if total is 6 and neither dice is 3
        System.out.println("Easy Six"); //prints slang terminology
      }
      else if ( diceTotal == 7 ){ //if statement for if total is 7
        System.out.println("Seven Out"); //prints slang terminology
      }
      else if ( firstDie == 4 && secondDie == 4 ){ //if statement for if total is 8 and both dice are 4
        System.out.println("Hard Eight"); //prints slang terminology
      }
      else if ( firstDie == 2 && secondDie == 6 || firstDie == 6 && secondDie == 2 || firstDie == 3 && secondDie == 5 || firstDie == 5 && secondDie == 3){ //if statement for if total is 8 and neither dice is 4
        System.out.println("Easy Eight"); //prints slang terminology
      }
      else if ( diceTotal == 9 ){ //if statement for if total is 9
        System.out.println("Nine"); //prints slang terminology
      }
      else if ( firstDie == 5 && secondDie == 5 ){ //if statement for if total is 10 and both dice are 5
        System.out.println("Hard Ten"); //prints slang terminology
      }
      else if ( firstDie == 4 && secondDie == 6 || firstDie == 6 && secondDie == 4 ){ //if statement for if total is 10 and neither dice is 5
        System.out.println("Easy Ten"); //prints slang terminology
      }
      else if ( diceTotal == 11 ){ //if statement for if total is 11
        System.out.println("Yo-leven"); //prints slang terminology
      }
      else if ( diceTotal == 12 ){ //if statement for if total is 12
        System.out.println("Boxcars"); //prints slang terminology
      }
      
    }  

  }//end of main method
}//end of class