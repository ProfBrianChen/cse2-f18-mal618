////////////
///Matthew Liu
///10/4/18
///CSE2 Course Questions

//Asks user for course information and checks to make sure it's of the correct type

import java.util.Scanner;
public class CourseQuestions{
  //maind method
  public static void main(String[] args) {
    
    Scanner scan = new Scanner( System.in );
    System.out.print("Enter course number: ");
    boolean correct = scan.hasNext();
    
    while (correct == false){
      System.out.println("Error: String type needed");
      System.out.print("Enter course number: ");
      correct = scan.hasNext();
      if (correct == true){
        String courseNumber = scan.next();
      }
      else{
        scan.next();//flushes out the non-String
      }
    }
    
    System.out.print("Enter department name: ");
    boolean correct = scan.hasNext();
    
    while (correct == false){
      System.out.println("Error: String type needed");
      System.out.print("Enter department name: ");
      correct = scan.hasNext();
      if (correct == true){
        String departmentName = scan.next();
      }
      else{
        scan.next();//flushes out the non-String
      }
    }
    
    System.out.print("Enter number of meetings per week: ");
    boolean correct = scan.hasNextInt();
    
    while (correct == false){
      System.out.println("Error: int type needed");
      System.out.print("Enter number of meetings per week: ");
      correct = scan.hasNextInt();
      if (correct == true){
        int numberMeetings = scan.nextInt();
      }
      else{
        scan.next();//flushes out the non-in
      }
    }
    
    System.out.print("Enter class start time: ");
    boolean correct = scan.hasNext();
    
    while (correct == false){
      System.out.println("Error: String type neede");
      System.out.print("Enter class start time: ");
      correct = scan.hasNext();
      if (correct == true){
        String startTime = scan.next();
      }
      else{
        scan.next();//flushes out the non-String
      }
    }
    
    System.out.print("Enter instructor name: ");
    boolean correct = scan.hasNext();
    
    while (correct == false){
      System.out.println("Error: String type needed");
      System.out.print("Enter instructor name: ");
      correct = scan.hasNext();
      if (correct == true){
        String instructorName = scan.next();
      }
      else{
        scan.next();//flushes out the non-String
      }
    }
    
    System.out.print("Enter number of students: ");
    boolean correct = scan.hasNextInt();
    
    while (correct == false){
      System.out.println("Error: int type needed");
      System.out.print("Enter number of students: ");
      correct = scan.hasNextInt();
      if (correct == true){
        int numberStudents = scan.nextInt();
      }
      else{
        scan.next();//flushes out the non-int
      }
    }
  }//end of main method
}//end of class