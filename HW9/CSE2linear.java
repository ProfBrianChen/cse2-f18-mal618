//////////
///Matthew Liu
///11/23/18
///CSE2 Linear

//

import java.util.Scanner;//imports Scanner class
import java.util.Random;//imports Random class
public class CSE2linear{
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );//calls scanner
    System.out.println("Enter 15 ascending ints for final grades in CSE2:");//prompts user to input 15 ints
    int array[] = new int[15];//creates new array
    for( int i = 0; i < 15; i++ ){//for loop to check array follows 3 criteria
      boolean correctInt = myScanner.hasNextInt();
      while ( correctInt == false ){ //while statement to check if an integer is entered     
        System.out.println("Error: int type needed");
        myScanner.nextLine();//flushes out non integer
        correctInt = myScanner.hasNextInt(); //checks if input is an integer
      }
      int enteredInt = myScanner.nextInt(); //initializes enteredInt as the input
      while( enteredInt < 0 || enteredInt > 100 ){//checks if int is between 0-100
        System.out.println("Error: int between 0-100 needed");
        myScanner.nextInt();//flushes out non integer
      }
      if(i>0){
      while(array[i] > array[i-1]){//checks if 
        System.out.println("Error: int needs to be greater than previous int");
        myScanner.nextInt();//flushes out non integer
        }
      }
      array[i] = enteredInt;//if int meets all three criteria, becomes part of array
    }
    printArray(array);//calls printArray method
    System.out.print("Enter a grade to search for: ");//prompts user for grade
    int firstInt = myScanner.nextInt();//makes firstInt user input
    binarySearch(array, firstInt);// calls binarySearch method
    array = randomScrambling(array);//calls randomScrambling method
    System.out.print("Enter a grade to search for: ");//prompts user for grade
    int secondInt = myScanner.nextInt();//makes secondInt user input
    linearSearch(array, secondInt);// calls binarySearch method
  }
  public static void binarySearch(int[] array, int grade){//binarySearch method
    int low = 0;//declares variable
    int high = array.length - 1;//declares variable
    int counter = 0;//declares variable
    while( low <= high ){//does binary search
      int middle = (low + high)/2;//declares variable
      if( grade < array[middle]){
        high = middle - 1;
        counter++;
      }
      else if( grade > array[middle]){
        low = middle + 1;
        counter++;
      }
      else{//if grade is found
        counter++;
        System.out.println(grade + " was found in the list with " + counter + " iterations");
        break;
      }
    }
    if( low > high ){//if grade isnt found
      System.out.println(grade + " was not found in the list with " + counter + " iterations");
    }
  }
  public static void linearSearch(int[] array, int grade){//linearSearch method
    int counter = 0;//declares variable
    for( int i = 0; i < array.length; i++ ){//for loop that checks each grade
      counter++;
      if( grade == array[i] ){//if grade is found
        System.out.println(grade + " was found in the list with " + counter + " iterations");
        break;
      }
      else if( i == array.length - 1 ){//if grade isnt found
        System.out.println(grade + " was not found in the list with " + counter + " iterations");
      }
    }
    
  }
  public static void printArray(int[] array){//printArray method
    for( int i = 0; i < array.length; i++ ){//prints array
      System.out.print(array[i] + " ");
    }
    System.out.println();//prints line
  }
  public static int[] randomScrambling(int[] array){//randomScrambling method
    Random scrambler = new Random();//calls Random class
    for(int i = 0; i < 60; i++){//for loop scrambles numbers 60 times
     int number = scrambler.nextInt(14)+1;//number is any number from 1-14
     int scrambleNumber = array[number];//switches card
     array[number] = array[0];//chosen card is now first card
     array[0] = scrambleNumber;//first card is now chosen card
  }
    System.out.println("Scrambled:");//prints Scrambled
    printArray(array);//calls printArray method
    return array;//returns scrambled array
  }
}

