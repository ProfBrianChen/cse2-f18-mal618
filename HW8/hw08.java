import java.util.Scanner;
public class hw08{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in);
     //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};//ranks 2 through ace
    String[] cards = new String[52];//creates array of 52 cards
    String[] hand = new String[5];//creates array of 5 cards for a hand
    int numCards = 5;//numCards is 5
    int again = 1;//again is 1
    int index = 51;//index is 51
    for (int i=0; i<52; i++){//for statement that initializes each member of array cards 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
    } 
    printArray(cards); //calls printArray method and prints unshuffled deck
    shuffle(cards); //calls shuffle
    printArray(cards); ///calls printArray method and prints shuffled deck
    while(again == 1){ //while loop that continues if again is 1
       if(numCards>index){//checks if numCards is greater than index
         shuffle(cards);//calls shuffle method
         index = 51;//index resets to 51
         printArray(cards);//calls printArray method
       }
       hand = getHand(cards,index,numCards); //calls getHand method
       printArray(hand);//calls printArray method
       index = index - numCards;//subtracts index by numCards
       System.out.println("Enter a 1 if you want another hand drawn"); //Asks user if another hand should be drawn
       again = scan.nextInt(); //user input
    }  
  }
  public static void printArray(String[] list){//printArray method
    for(int i = 0; i < list.length; i++){//for loop continues for length of list
      System.out.print(list[i] + " ");//prints each card
    }
    System.out.println("");
  }
  public static void shuffle(String[] list){//shuffle method
    for(int j = 0; j < 100; j++){//for loop shuffles cards 100 times
      int card = (int)(Math.random()*51)+1;//card is any number from 1-52
      String switchCard = list[card];//switches card
      list[card] = list[0];//chosen card is now first card
      list[0] = switchCard;//first card is now chosen card
    }
    System.out.println("Shuffle");//print "Shuffle"
  }
  public static String[] getHand(String[] list, int index, int numCards){//getHand method
    System.out.println("Hand");//prints "Hand"
    String[] card = new String[numCards];//declares array card
    for(int i = 0; i < numCards; i++){//for loop continues for numCards
      card[i] = list[index - i];//each card is drawn from the end of list
    }
    return card;//returns card
  }
  
}
