/////////////
////Matthew Liu
////10/29/18
////CSE2 Hw07

//Prompts user to choose whether to count the number of non-whitespace characters, number of words, find text, replace all !'s, shorten spaces, or quit

import java.util.Scanner;//imports scanner class
public class Hw07{
  //main method
  public static void main(String[] args){
    String savedString = sampleText();//saves user input
    boolean optionChosen = false;
    while(optionChosen == false){//while statement that calls printMenu method
      String letterCommand = printMenu();//calls printMenu method
      switch( letterCommand ){//switch statement for different inputs
        case "c"://case if user enters c
          int characterNumber = getNumOfNonWSCharacters( savedString );//calls getNumOfNonWSCharacters method
          System.out.println( "Number of non-whitespace characters: " + characterNumber );//prints number of non whitespace characters
          break;
        case "w"://case if user enters w
          int wordCount = getNumOfWords( savedString );//calls getNumOfWords method
          System.out.println( "Number of words: " + wordCount );//prints word count
          break;
        case "f"://case if user enters f
          System.out.print("Enter a word or phrase to be found: ");//prompts user to enter a word or phrase
          Scanner myScanner = new Scanner( System.in );//calls scanner
          String findWord = myScanner.nextLine();//declares scanner
          int wordInstances = findText( findWord, savedString );//calls findText method
          System.out.println("\"" + findWord + "\" instances: " + wordInstances );//prints instances of word found
          break;
        case "r"://case if user enters r
          String newText = replaceExclamation( savedString );//calls replaceExclamation method
          System.out.println( "Edited text: " + newText );//prints new String
          break;
        case "s"://case if user enters s
          String editedText = shortenSpace( savedString );//calls shortenSpace method
          System.out.println( editedText );//prints new String
          break;
        case "q"://case if user enters q
          optionChosen = true;//quits menu
          break;
        default://ends switch statement. Main menu prints again
          break;
      }
    }
    
    
  }//end of main method
  
  public static String sampleText(){//sampleText method
    System.out.println("Enter a String of your choosing:");//prompts user to choose an option
    Scanner myScanner = new Scanner( System.in );//calls scanner
    String textInput = myScanner.nextLine();//declares Scanner
    System.out.println( "You entered: " + textInput );//prints user input
    return( textInput );//returns user input
  }
  
  public static String printMenu(){//printMenu method
    System.out.println("Menu");//prints menu
    System.out.println("c - Number of non-whitespace characters");//prints option
    System.out.println("w - Number of words");//prints option
    System.out.println("f - Find text");//prints option
    System.out.println("r - Replace all !'s");//prints option
    System.out.println("s - Shorten spaces");//prints option
    System.out.println("q - Quit");//prints option
    System.out.println("");//skips line
    System.out.println("Choose an option:");//prompts user to choose an option
    Scanner myScanner = new Scanner( System.in );//calls scanner
    String letterInput = myScanner.next();//declares scanner
    return( letterInput );//returns letter user input
  }
  
  public static int getNumOfNonWSCharacters( String sampleText ){//getNumOfNonWSCharacters method
    int characterNumber = sampleText.length();//gets length of sampleText
    for( int i = 0; i <= characterNumber; i++){//for loop to check for whitespace
      char currentChar = sampleText.charAt(i);//gives current char at i
      switch( currentChar ){//switch statement to check if current char is a whitespace
        case ' '://case if whitespace
          characterNumber--;//characterNumber goes down by 1
          break;
      }
    }
    return(characterNumber);//returns characterNumber
  }
  
  public static int getNumOfWords( String sampleText ){//getNumOfWords method
    int characterNumber = sampleText.length();//gets length of sampleText
    int wordCount = 0;//word count starts at 0
    for( int i = 0; i < characterNumber; i++ ){//for loop to check for word endings
      char currentChar = sampleText.charAt(i);//gives current char at i
      switch( currentChar ){//switch statement to check if current char is whitespace or ends sentence
        case '.'://adds one word
          wordCount++;
          break;
        case ','://adds one word
          wordCount++;
          break;
        case '!'://adds one word
          wordCount++;
          break;
        case '?'://adds one word
          wordCount++;
          break;
        case ':'://adds one word
          wordCount++;
          break;
        case ';'://adds one word
          wordCount++;
          break;
        case ' ':
          char previousChar = sampleText.charAt(i-1);//checks previous character
          switch( previousChar ){//switch statement that doesn't add 1 to word count if previous char was also whitespace or ended sentence
            case ' ':
              break;
            case '.':
              break;
            case ',':
              break;
            case '!':
              break;
            case '?':
              break;
            case ':':
              break;
            case ';':
              break;
            default://adds one word
              wordCount++;
              break;
          }
          break;
      }
    }
    return(wordCount);//returns wordCount
  }
  
  public static int findText( String findThisWord, String sampleText ){//findText method
    int wordLength = findThisWord.length();//gives length of  findThisWord
    int textLength = sampleText.length();//gives length of sampleText
    int wordCounter = 0;//word counter starts at 0
    for(int i = 0; i + wordLength <= textLength; i++ ){//for loop to check if there is a matching word or phrase
      boolean wordMatch = findThisWord.equals(sampleText.substring( i , i + wordLength ));//checks if two strings are equal
      if( wordMatch == true ){//if two strings are equal, wordCounter goes up 1
         wordCounter++;
      }
    }
    return(wordCounter);//return wordCounter
  }
  
  public static String replaceExclamation( String sampleText ){//replaceExclamation method
    String editedText = sampleText.replace( "!" , "." );//replaces ! with .
    return(editedText);//returns edited statement
  }
  
  public static String shortenSpace( String sampleText ){//shortenSpace method
    int characterNumber = sampleText.length();//gives length of sampleText
    String editedText = "Edited text: ";//original edited text
      for( int i = 0; i < characterNumber; i++ ){//for loop to check if there is more than one space
        char currentChar = sampleText.charAt(i);//gives current char
        switch( currentChar ){//switch statement to check for spaces
          case ' '://case for a space
            char previousChar = sampleText.charAt(i-1);//gives previous character
            switch( previousChar ){//checks previous character
              case ' '://case if previous character was a space
                break;//does not get added to edited text
              default://if not, keep the space
                editedText+=currentChar;
                break;
            }
            break;
          default://adds chars if they are not spaces
            editedText+=currentChar;
            break;
        }
      }
    return(editedText);//returns editedText
  }
  

  
}//end of class