/////////////////
/////Matthew Liu
////10/7/18
////CSE2 Hw05

//Asks user how many times it should generate hands
//For each hand, randomly generates five cards

import java.util.Scanner; //imports scanner class
public class Hw05{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); //declares scanner
    
    int handsTotal = 1; //declares variable for total number of hands
    int handsCounter = 0; //declares hand counter
    boolean correctInt = false; //declares boolean correctInt to check if an integer is entered
    
    while ( correctInt == false ){ //while statement to check if an integer is entered
      System.out.println("How many hands would you like to generate?"); //asks how many hands user would like to generate
      correctInt = myScanner.hasNextInt(); //checks if input is an integer
      if( correctInt == true ){ 
        handsTotal = myScanner.nextInt(); //if true initializes handsTotalotal as the input
      }
      else{
        System.out.println("Error: int type needed");
        myScanner.nextInt();//flushes out non integer
      }
    }
    int fourOfAKind = 0, threeOfAKind = 0, twoPair = 0, onePair = 0;//declares variables
    int fifthCard = 0, fourthCard = 0, thirdCard = 0, secondCard = 0, firstCard = 0;//declares variables
    int firstValue = 0, secondValue = 0, thirdValue = 0, fourthValue = 0, fifthValue = 0;//declares variables
;
    while ( handsCounter < handsTotal ){ //loops while handsCounter is less than handsTotal
      firstCard = (int)(Math.random()*51)+1; //picks any integer from 1-52
      secondCard = (int)(Math.random()*51)+1;//picks any integer from 1-52
      thirdCard = (int)(Math.random()*51)+1;//picks any integer from 1-52
      fourthCard = (int)(Math.random()*51)+1;//picks any integer from 1-52
      fifthCard = (int)(Math.random()*51)+1;//picks any integer from 1-52

      while (secondCard == firstCard){ //makes sure no cards are the same
        secondCard = (int)(Math.random()*51)+1; //re-picks any integer from 1-52    
      }
      while (thirdCard == firstCard || thirdCard == secondCard){ //makes sure no cards are the same
        thirdCard = (int)(Math.random()*51)+1; //re-picks any integer from 1-52  
      }
      while (fourthCard == firstCard || fourthCard == secondCard || fourthCard == thirdCard){ //makes sure no cards are the same
        fourthCard = (int)(Math.random()*51)+1; //re-picks any integer from 1-52  
      }
      while (fifthCard == firstCard || fifthCard == secondCard || fifthCard == thirdCard || fifthCard == fourthCard){ //makes sure no cards are the same
        fifthCard = (int)(Math.random()*51)+1; //re-picks any integer from 1-52  
      }
      
      firstValue = firstCard%13; //finds value of card from ace through king
      secondValue = secondCard%13; //finds value of card from ace through king
      thirdValue = thirdCard%13; //finds value of card from ace through king
      fourthValue = fourthCard%13;//finds value of card from ace through king
      fifthValue = fifthCard%13; //finds value of card from ace through king
      
      if (firstValue == secondValue && secondValue == thirdValue && thirdValue == fourthValue){ //else statement for four of a kind
        fourOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == thirdValue && thirdValue == fifthValue){ //else statement for four of a kind
        fourOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == fourthValue && fourthValue == fifthValue){ //else statement for four of a kind
        fourOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fourthValue && fourthValue == thirdValue && thirdValue == fifthValue){ //else statement for four of a kind
        fourOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fourthValue == secondValue && secondValue == thirdValue && thirdValue == fifthValue){ //else statement for four of a kind
        fourOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == thirdValue && fourthValue == fifthValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == fourthValue && thirdValue == fifthValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == fifthValue && thirdValue == fourthValue){ //else statement for full house
        threeOfAKind++;
        onePair++;
        handsCounter++;
      }
      else if (firstValue == thirdValue && thirdValue == fourthValue && secondValue == fifthValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == thirdValue && thirdValue == fifthValue && secondValue == fourthValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fourthValue && fourthValue == fifthValue && thirdValue == secondValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (thirdValue == secondValue && secondValue == fourthValue && firstValue == fifthValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fifthValue == secondValue && secondValue == fourthValue && firstValue == thirdValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (thirdValue == fourthValue && fifthValue == fourthValue && firstValue == secondValue){ //else statement for full house
        threeOfAKind++;//increases by 1
        onePair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == thirdValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == fourthValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && secondValue == fifthValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fourthValue && fourthValue == thirdValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fourthValue && fourthValue == fifthValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fifthValue && fifthValue == thirdValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fourthValue == secondValue && secondValue == thirdValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fifthValue == secondValue && secondValue == thirdValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fourthValue == secondValue && secondValue == fifthValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fourthValue == fifthValue && fifthValue == thirdValue){ //else statement for three of a kind
        threeOfAKind++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && fourthValue == thirdValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == thirdValue && fourthValue == fifthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && fourthValue == fifthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue && thirdValue == fifthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == thirdValue && secondValue == fifthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == thirdValue && fourthValue == secondValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fourthValue && secondValue == fifthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fourthValue && secondValue == thirdValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fourthValue && thirdValue == fifthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fifthValue && secondValue == thirdValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fifthValue && secondValue == fourthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == fifthValue && fourthValue == thirdValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fourthValue == fifthValue && secondValue == thirdValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fourthValue == thirdValue && secondValue == fifthValue){ //else statement for two pair
        twoPair++;//increases by 1
        handsCounter++;//increases by 1
      }
      else if (fourthValue == secondValue && fifthValue == thirdValue){ //else statement for two pair
        twoPair++; //increases by 1
        handsCounter++;//increases by 1
      }
      else if (firstValue == secondValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (firstValue == thirdValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (firstValue == fourthValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (firstValue == fifthValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (thirdValue == secondValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (fourthValue == secondValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (fifthValue == secondValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (thirdValue == fourthValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (thirdValue == fifthValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
      else if (fifthValue == fourthValue){ //else statement for one pair
        onePair++; //onePair increases by 1
        handsCounter++; //handsCounter increases by 1
      }
    }
    double fourFraction = (double)fourOfAKind/(double)handsTotal; //divides fourOfAKind by total number of hands
    double fourProbability = ((double)(int)((fourFraction)*1000)/1000); //finds probability of four of a kind to three decimals
    double threeFraction = (double)threeOfAKind/(double)handsTotal; //divides threeOfAKind by total number of hands
    double threeProbability = ((double)(int)((threeFraction)*1000)/1000); //finds probability of three of a kind to three decimals
    double twoFraction = (double)twoPair/(double)handsTotal; //divides twoPair by total number of hands
    double twoProbability = ((double)(int)((twoFraction)*1000)/1000); //finds probability of two pair to three decimals
    double oneFraction = (double)onePair/(double)handsTotal; //divides onePair by total number of hands
    double oneProbability = ((double)(int)((oneFraction)*1000)/1000); //finds probability of one pair to three decimals
    System.out.println("The number of loops: " + handsTotal); //prints total number of loops
    System.out.println("The probability of Four-of-a-kind: " + fourProbability); //prints probability of four of a kind
    System.out.println("The probability of Three-of-a-kind: " + threeProbability); //prints probability of three of a kind
    System.out.println("The probability of Two-pair: " + twoProbability); //prints probability of two-pair
    System.out.println("The probability of One-pair: " + oneProbability); //prints probability of one-pair
  }//end of main method
}//end of class