//////////////
///Matthew Liu
///10/11/18
///CSE2 Lab 6

//Asks user for an integer between 1-10
//Prints pyramid in a certain pattern with the number of rows entered

import java.util.Scanner;

public class PatternD{
  public static void main(String[] args){
    //main method
    Scanner myScanner = new Scanner( System.in );
    
    int enteredValue = 1;
    boolean correctInt = false; //declares boolean correctInt to check if an integer is entered
    
    while ( correctInt == false ){ //while statement to check if an integer is entered
      System.out.print("Enter an integer between 1 and 10: "); //asks user to enter integer between 1 and 10
      correctInt = myScanner.hasNextInt(); //checks if input is an integer
      if( correctInt == true ){ 
        enteredValue = myScanner.nextInt(); //if true, initializes enteredValue as the input
      }
      else{
        System.out.println("Error: int type needed");// prints in type needed
        myScanner.nextLine();//flushes out non integer
      }
    }
    
    while (1>enteredValue || enteredValue>10){ //while statement for if enteredValue is an integer not between 1 and 10
        System.out.print("Error: Please enter an integer between 1-10: ");//prompts for value between 1 and 10
        enteredValue = myScanner.nextInt();//initializes enteredValue as the input
    }
    
    int i, j;// declares integers i and j
    for ( i = enteredValue; i >= 1; --i ){//adds one to i until it reaches enteredValue
      //loop body
      for ( j = i; j >= 1; --j ){//adds one to j until it reaches i
        System.out.print( j ); //prints j
      }
      System.out.println(" ");//jumps to next line of pyramid
    }
    
  }//end of main method
}//end of class