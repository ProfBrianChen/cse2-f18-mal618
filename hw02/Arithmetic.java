//////////////
////Matthew Liu
////9/6/18
////CSE2 Arithmetic
///
public class Arithmetic {
  //Calculates cost of each item as well as sales tax
  //Calculates total cost before tax, total sales tax, and total cost after tax
  public static void main(String args[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //output variables
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts; //total cost of each item
    double salesTaxPants, salesTaxShirts, salesTaxBelts; //sales tax charged on each item
    double totalCostBefore; //total cost before tax
    double totalSalesTax; //total sales tax of all items
    double totalCostAfter; //total cost after tax
    
    //Calculates total cost of each item
    totalCostOfPants = pantsPrice*numPants;
    totalCostOfShirts = shirtPrice*numShirts;
    totalCostOfBelts = beltCost*numBelts;
    
    //Prints total cost of each item
    System.out.println("Total cost of Pants: "+totalCostOfPants);
    System.out.println("Total cost of Sweatshirts: "+totalCostOfShirts);
    System.out.println("Total cost of Belts: "+totalCostOfBelts);
    
    //Calculates sales tax of each item
    salesTaxPants = 100*paSalesTax*totalCostOfPants;
    salesTaxPants = ((double)(int)salesTaxPants)/100;
    salesTaxShirts = 100*paSalesTax*totalCostOfShirts;
    salesTaxShirts = ((double)(int)salesTaxShirts)/100;
    salesTaxBelts = 100*paSalesTax*totalCostOfBelts;
    salesTaxBelts = ((double)(int)salesTaxBelts)/100;
    
    //Prints cost of sales tax for each item
    System.out.println("Sales tax for Pants: "+salesTaxPants);
    System.out.println("Sales tax for Sweatshirts: "+salesTaxShirts);
    System.out.println("Sales tax for Belts: "+salesTaxBelts);
    
    //Calculates total cost before tax, total sales tax, and total cost after tax
    totalCostBefore = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
    totalCostAfter = totalCostBefore + totalSalesTax;
    
    //Prints total cost before tax, total sales tax, and total cost after tax
    System.out.println("Total cost before tax: "+totalCostBefore);
    System.out.println("Total cost of sales tax: "+totalSalesTax);
    System.out.println("Total cost after tax: "+totalCostAfter);
  }//end of calculation
}//end of class


