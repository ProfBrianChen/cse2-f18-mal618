///////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    /// prints welcome message and Lehigh ID to terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-M--A--L--6--1--8->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
                       
    System.out.println("My is Matthew Liu and I'm a High School Scholar at Lehigh University. I'm a senior at Parkland High School. I hope to major in biochemistry.");
    
    
  }
  
}