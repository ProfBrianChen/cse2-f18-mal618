//////////////
////Matthew Liu
////10/21/18
////CSE2 EncryptedX

//Asks user for integer between 0-100 and checks if input is an integer between 0-100
//Hides the secret message X in a grid the size of the integer input

import java.util.Scanner;//imports scanner class
public class EncryptedX{
  //main method
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in );//declares scanner
    
    int myValue = 1;//declares myValue
    boolean correctInt = false;//declares correctInt
    while ( correctInt == false ){//while statement that checks if input is an integer
      System.out.print("Enter an integer between 0-100: ");//prompts user for an integer between 0-100
      correctInt = myScanner.hasNextInt();
      if ( correctInt == true ){//runs if correctInt is true
        myValue = myScanner.nextInt();//myValue becomes what user enters
        while ( myValue > 100 || myValue < 0 ){//checks if integer input is between 0-100
          System.out.print("Enter an integer between 0-100: ");//prompts user for an integer between 0-100
          myValue = myScanner.nextInt();
        }
      }
      else{//flushes out non-integer
        myScanner.nextLine();
      }
    }
    
    int i, j;//declares integers i and j
    for ( i = 0; i < myValue; i++ ){//increases i by 1 until it reaches myValue
      for ( j = 0; j < myValue; j++ ){//increases j by 1 until it reaches myValue
        if ( j == i ){//prints space when j = 1
          System.out.print(" ");
        }
        else if (  j == (myValue-1) - i ){//prints space when myValue - i +1 is j
          System.out.print(" ");
        }
        else {//prints * in all other places
          System.out.print("*");
        } 
      }
      System.out.println("");//skips to next line
    }
    
    
  }//end of main method
}//end of class
