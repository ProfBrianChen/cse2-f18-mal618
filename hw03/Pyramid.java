//////////
///Matthew Liu
///9/17/18
///CSE Pyramid
//

//Uses scanner class to obtain measurements for length and height of the pyramid
//Calculates volume of the pyramid

import java.util.Scanner; //imports scanner class

public class Pyramid{
  //main method
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); //declares object
    System.out.print("The square side of the pyramid is (input length): "); //prints "The square side of the pyramid is (input length): "
    int pyramidLength = myScanner.nextInt(); //constructs instance
    
    System.out.print("The height of the pyramid is (input height): "); //prints "The height of the pyramid is (input height): "
    int pyramidHeight = myScanner.nextInt(); //constructs instance
    
    int pyramidVolume = (int)(pyramidLength * pyramidLength * pyramidHeight)/3; //calculates the volume of the pyramid
    System.out.println("The volume in the pyramid is: " + pyramidVolume); //prints pyramid volume
      
      
  }//end of main method
}//end of class