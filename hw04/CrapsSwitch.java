////////////////
///Matthew Liu
///9/24/18
///CSE2 Craps Switch

//Asks user if they want to randomly cast dice or state the two dice they want evaluated
//Using switch statements, either randomly generates two integers 1-6, or asks for two integers 1-6 using switch statements
//Prints slang terminology for outcome of roll

import java.util.Scanner;
public class CrapsSwitch{
  //main method
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); //declares scanner class
    System.out.print("Would you like your dice cast to be random? (Enter 1 for yes, 2 for no):"); //prints question statement
    int yesNo = myScanner.nextInt(); //constructs instance
    
    switch ( yesNo ) { //switch statement
      case 1: //case for if yesNo is 1
        int firstDie = (int)(Math.random()*5)+1; //randomly selects integer between 1 and 6 for first die
        int secondDie = (int)(Math.random()*5)+1; //randomly selects integer between 1 and 6 for second die
        int diceTotal = firstDie + secondDie; //adds value for first and second die to get dice total
        switch ( diceTotal ){ //switch statement for dice total
          case 2: //case for if dice total is 2
            System.out.println("Snake Eyes"); //prints slang terminology
            break;
          case 3: //case for if dice total is 3
            System.out.println("Ace Deuce"); //prints slang terminology
            break;
          case 5: //case for if dice total is 5
            System.out.println("Fever Five"); //prints slang terminology
            break;
          case 7: //case for if dice total is 7
            System.out.println("Seven Out"); //prints slang terminology
            break;
          case 9: //case for if dice total is 9
            System.out.println("Nine"); //prints slang terminology
            break;
          case 11: //case for if dice total is 11
            System.out.println("Yo-leven"); //prints slang terminology
            break;
          case 12: //case for if dice total is 12
            System.out.println("Boxcars"); //prints slang terminology
            break;
          default:
            switch ( firstDie ){ //switch statement for first die
              case 1: //case when first die is 1
                switch ( secondDie ){ //switch statement for second die
                  case 3: //case when second die is 3
                    System.out.println("Easy Four"); //prints slang terminology
                    break;
                  case 5: //case when second die is 5
                    System.out.println("Easy Six"); //prints slang terminology
                    break;
                }
                break;
              case 2: //case when first die is 2
                switch ( secondDie ){ //switch statement for second die
                  case 2: //case when second die is 2
                    System.out.println("Hard Four"); //prints slang terminology
                    break;
                  case 4: //case when second die is 4
                    System.out.println("Easy Six"); //prints slang terminology
                    break;
                  case 6: //case when second die is 6
                    System.out.println("Easy Eight"); //prints slang terminology
                    break;
                }
                break;
              case 3: //case for easy six
                switch ( secondDie ){ //switch statement for second die
                  case 3: //case when second die is 3
                    System.out.println("Hard Six"); //prints slang terminology
                    break;
                  case 5: //case when second die is 5
                    System.out.println("Easy Eight"); //prints slang terminology
                    break;
                }
                break;
              case 4: //case for hard six
                 switch ( secondDie ){ //switch statement for second die
                  case 4: //case when second die is 4
                    System.out.println("Hard Eight"); //prints slang terminology
                    break;
                  case 6: //case when second die is 6
                    System.out.println("Easy Ten"); //prints slang terminology
                    break;
                 }
              case 5: //case for easy eight
                 switch ( secondDie ){ //switch statement for second die
                  case 5: //case when second die is 5
                    System.out.println("Hard Ten"); //prints slang terminology
                    break;
                 }
            }
        }
        break;
      case 2: //case for if yesNo is 2
        System.out.print("Enter value of first die. (integer between 1 and 6):"); //prints request for value of first die
        int firstInt = myScanner.nextInt(); //constructs instance
        System.out.print("Enter value of second die. (integer between 1 and 6):"); //prints request for value of second die
        int secondInt = myScanner.nextInt(); //constructs instance
            switch ( firstInt ){ //switch statement for first die
              case 1: //case when first die is 1
                switch ( secondInt ){ //switch statement for second die
                  case 1: //case when second die is 1
                    System.out.println("Snake Eyes"); //prints slang terminology
                    break;
                  case 2: //case when second die is 2
                    System.out.println("Ace Deuce"); //prints slang terminology
                    break;
                  case 3: //case when second die is 3
                    System.out.println("Easy Four"); //prints slang terminology
                    break;
                  case 4: //case when second die is 4
                    System.out.println("Fever Five"); //prints slang terminology
                    break;
                  case 5: //case when second die is 5
                    System.out.println("Easy Six"); //prints slang terminology
                    break;
                  case 6: //case when second die is 6
                    System.out.println("Seven Out"); //prints slang terminology
                    break;
                }
                break;
              case 2: //case when first die is 2
                switch ( secondInt ){ //switch statement for second die
                  case 2: //case when second die is 2
                    System.out.println("Hard Four"); //prints slang terminology
                    break;
                  case 3: //case when second die is 3
                    System.out.println("Fever Five"); //prints slang terminology
                    break;
                  case 4: //case when second die is 4
                    System.out.println("Easy Six"); //prints slang terminology
                    break;
                  case 5: //case when second die is 5
                    System.out.println("Seven Out"); //prints slang terminology
                    break;
                  case 6: //case when second die is 6
                    System.out.println("Easy Eight"); //prints slang terminology
                    break;
                }
                break;
              case 3: //case for easy six
                switch ( secondInt ){ //switch statement for second die
                  case 3: //case when second die is 3
                    System.out.println("Hard Six"); //prints slang terminology
                    break;
                  case 4: //case when second die is 4
                    System.out.println("Seven Out"); //prints slang terminology
                    break;
                  case 5: //case when second die is 5
                    System.out.println("Easy Eight"); //prints slang terminology
                    break;
                  case 6: //case when second die is 6
                    System.out.println("Nine"); //prints slang terminology
                    break;
                }
                break;
              case 4: //case for hard six
                 switch ( secondInt ){ //switch statement for second die
                  case 4: //case when second die is 4
                    System.out.println("Hard Eight"); //prints slang terminology
                    break;
                  case 5: //case when second die is 5
                    System.out.println("Nine"); //prints slang terminology
                    break;
                  case 6: //case when second die is 6
                    System.out.println("Easy Ten"); //prints slang terminology
                    break;
                 }
              case 5: //case for easy eight
                 switch ( secondInt ){ //switch statement for second die
                  case 5: //case when second die is 5
                    System.out.println("Hard Ten"); //prints slang terminology
                    break;
                  case 6: //case when second die is 6
                    System.out.println("Yo-leven"); //prints slang terminology
                    break;
                 }
              case 6: //case for easy eight
                 switch ( secondInt ){ //switch statement for second die
                  case 6: //case when second die is 6
                    System.out.println("Boxcars"); //prints slang terminology
                    break;
                 }    
              default:
                 System.out.println("Incorrect integer input"); //prints statement if 1 through 6 is not the input    
            }
        break;
      default:
        System.out.println("Incorrect integer input"); //prints statement if neither 1 or 2 is the input
        break;
    }
 
  }//end of main method
}//end of class