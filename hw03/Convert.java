//////////
///Matthew Liu
///9/17/18
///CSE2 Convert
//

//Uses scanner class to obtain affected area and rainfall
//Calculates how many cubic miles of rain

import java.util.Scanner; //imports scanner class

public class Convert{
  //main method
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); //declares object
    System.out.print("Enter the affected area in acres: "); //prints "Enter the affected area in acres: "
    double area = myScanner.nextDouble(); //constructs instance
    double squareMilesPerAcre = 0.0015625; //conversion for acres to square miles
    
    System.out.print("Enter the rainfall in the affected area: "); //prints "Enter the rainfall in the affected area: "
    double rainfall = myScanner.nextDouble(); //constructs instance
    double inchesPerFoot = 12; //conversion for inches to feet
    double feetPerMile = 5280; //conversion for feet to mile
    
    double squareMiles = area * squareMilesPerAcre; //coverts area to square miles
    double milesOfRain = rainfall / inchesPerFoot / feetPerMile; //converts rainfall to miles
    float cubicMiles = (float)(squareMiles * milesOfRain); //calculates cubic miles of rain
    
    System.out.println(cubicMiles + " cubic miles"); //prints cubic miles of rainfall
    
    
  }//end of main method
}//end of class