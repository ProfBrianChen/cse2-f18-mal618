//////////////////////
////Matthew Liu
////9-6-18
////CSE2 Cyclometer
///
public class Cyclometer {
  //calculates how long and how many revolutions Trip1 and Trip2 took; calculates distance of Trip1, Trip2, and total distance 
  //prints calculations to terminal window
  public static void main(String args[]){
    
    //input data
    int secsTrip1=480;  //number of seconds for trip 1
    int secsTrip2=3220; //number of seconds for trip 2
    int countsTrip1=1561; //number of rotations for trip 1
    int countsTrip2=9037; //number of rotations for trip 2
    
    //intermediate variables and output data
    double wheelDiameter=27.0, //diameter of wheel
    PI=3.14159, //value of PI
    feetPerMile=5280, //conversion for feet to mile
    inchesPerFoot=12, //conversion for inches to feet
    secondsPerMinute=60; //conversion for seconds to minutes
    double distanceTrip1, distanceTrip2, totalDistance; //variables for distance of Trip1, Trip2, and total
    
      //print number of minutes and counts
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    
      //runs calculations; stores values; calculates distanceTrip1, distanceTrip2, totaldistance
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	//above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//gives distanceTrip2 in miles
	  totalDistance=distanceTrip1+distanceTrip2;//gives total distance
    
      //print out the output data
    System.out.println("Trip1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
 
    
    
  } // end of calculation
} //end of class