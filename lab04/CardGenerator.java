//////////
//Matthew Liu
//9/20/18
//CSE2 Card Generator
//

//Using Math.random, generate a random card from a deck

public class CardGenerator{
  //main method
  public static void main(String[] args){
    
    int card = (int)(Math.random()*51)+1;
    String diamonds, clubs, hearts, spades;
    
    switch( card%13 ){
      case 1:
        System.out.print("You picked the Ace ");
        break;
        
      case 2:
        System.out.print("You picked the 2 ");
        break;
        
      case 3:
        System.out.print("You picked the 3 ");
        break;
        
      case 4:
        System.out.print("You picked the 4 ");
        break;
        
      case 5:
        System.out.print("You picked the 5 ");
        break;
      
      case 6:
        System.out.print("You picked the 6 ");
        break;
        
      case 7:
        System.out.print("You picked the 7 ");
        break;
        
      case 8:
        System.out.print("You picked the 8 ");
        break;
        
      case 9:
        System.out.print("You picked the 9 ");
        break;
        
      case 10:
        System.out.print("You picked the 10 ");
        break;
        
      case 11:
        System.out.print("You picked the Jack ");
        break;
        
      case 12:
        System.out.print("You picked the Queen ");
        break;
        
        case 13:
        System.out.print("You picked the King ");
        break;
        
      default:
        System.out.print("You did not draw a card");
    }
    
    if (card <= 13){
      System.out.println("of Diamonds");
    }
    else if (card <= 26){
      System.out.println("of Clubs");
    }
    else if (card <= 39){
      System.out.println("of Hearts");
    }
    else if (card <= 52){
      System.out.println("of Spades");
    }
  }//end of main method
}//end of class