/////////////
///Matthew Liu
///11/23/18
///CSE2 RemoveElements

//

import java.util.Scanner;//imports Scanner class
public class RemoveElements{
  public static void main(String [] arg){//main method
	Scanner scan = new Scanner(System.in);//declares scanner
  int num[]=new int[10];//creates array
  int newArray1[];//creates array
  int newArray2[];//creates array
  int index,target;//creates variable
	String answer = "";//creates variable
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();//calls randomInput method
  	String out = "The original array is:";
  	out += listArray( num );
  	System.out.println( out );//prints array
 
  	System.out.print("Enter the index ");//prompts user to enter index
  	index = scan.nextInt();
  	newArray1 = delete( num,index );//calls delete method
  	String out1="The output array is ";
  	out1+=listArray( newArray1 ); //return a string of the form "{2, 3, -9}"  
  	System.out.println( out1 );// print new array
 
    System.out.print("Enter the target value ");//prompts user to enter target value
  	target = scan.nextInt();
  	newArray2 = remove( num,target );//calls remove method
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);//print new array
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");//prompts user if they want to go again
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
}
 
  public static String listArray(int num[]){//listArray method
	String out="{";
	for(int j=0;j<num.length;j++){//adds array to out
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;//returns out
  }
  
  public static int[] randomInput(){//randomInput method
    int array[] = new int[10];//declares int array
    for( int i = 0; i < 10; i++ ){//randomly selects ints from 0-9 for array
      array[i] = (int)(Math.random()*9);
    }
    return array;//returns array
  }
  
  public static int[] delete(int[] list, int pos){//delete method
    int newArray[] = new int[list.length - 1];//declares newArray
    int j = 0;
    for( int i = 0; i < 10; i++){
      if( i == pos ){//deletes array at pos
        --j;
      }
      else{//keeps other members of array
        newArray[j] = list[i];
      }
      j++;
    }
    return newArray;//returns newArray
  }
  public static int[] remove(int[] list, int target){//remove method
    int arrayLength = list.length;//declares variable
    for( int i = 0; i < list.length; i++ ){//for loop that shortens array
      if( list[i] == target){
        arrayLength--;
      }
    }
    int removedArray[] = new int[arrayLength];//declares removedArray array
    int j = 0;//declares variable
    for( int k = 0; k < list.length; k++ ){//makes removedArray list with target removed
      if( list[k] != target){
        removedArray[j] = list[k];
        j++;
      }
    }
    return removedArray;//returns removedArray
  }
}
